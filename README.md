# tradingview-indicators
a repository for tradingview indicators

These indicators are written in [Pine Script](https://www.tradingview.com/wiki/Pine_Script_Tutorial). Tradingview's proprietary language.

## How to use:

On tradingview.com, at the bottom you will see a tab named "Pine Script", click that to open a new script.  Paste any of the indicators located in the indicators folder into the new, empty pine script.  Hit the "Add to Chart" button in the upper right of the pine script viewport.  This will add the indicator to the current chart!

If you want to save any indicator, click on save button! 
