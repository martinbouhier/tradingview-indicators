// This source code is subject to the terms of the Mozilla Public License 2.0 at https://mozilla.org/MPL/2.0/
//@version=4

study(title="trendline", overlay=true)

FromYear = input(2020, "From Year", minval = 1900)
FromMonth = input(11, "From Month", minval = 1, maxval = 12)
FromDay = input(2, "From Day", minval = 1, maxval = 31)

ToYear = input(2021, "To Year", minval = 1900)
ToMonth = input(8, "To Month", minval = 1, maxval = 12)
ToDay = input(4, "To Day", minval = 1, maxval = 31)

FromDate = timestamp(FromYear, FromMonth, FromDay)
ToDate = timestamp(ToYear, ToMonth, ToDay)
var ln = line.new(x1=FromDate, y1=108,x2=ToDate, y2=126.40, xloc = xloc.bar_time)
